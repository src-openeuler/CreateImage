Name:        CreateImage
Version:     1.0.0
Release:     7
Summary:     The tool to create Machine Image
Group:       System Environment/Base
License:     Mulan PSL v2
Source0:     https://gitee.com/openeuler/%{name}/repository/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Patch0001:   0001-fix-uname-r-error.patch
Patch0002:   0001-Add-loongarch64-support-for-CreateImage.patch
Patch0003:   add-some-packages-in-rpmlist.patch
Patch0004:   fix-make-image-in-docker.patch
Patch0005:   0001-add-basic-riscv64-support.patch

BuildArch:   noarch
Requires:    qemu-img bc

%description
The tool to create Machine Image.

%prep
%setup -q
%patch0001 -p1
%patch0002 -p1
%patch0003 -p1
%patch0004 -p1
%patch0005 -p1

%define env_kernel_version %(eval rpm -qi kernel-headers  | grep Version | awk '{print $NF}')
%define enable_nomodeset %(eval echo %{env_kernel_version} 6.1.0 | awk '{if($1 >= $2) print 1;else print 0;}')
%if "%{enable_nomodeset}" == "1"
	sed -i 's/nomodeset//g' hooks/finalise.d/50-bootloader
%endif

%build

%install
mkdir -p %{buildroot}/usr/bin/
cp bin/* %{buildroot}/usr/bin/
mkdir -p %{buildroot}/usr/share/CreateImage
cp -a lib hooks config %{buildroot}/usr/share/CreateImage

%clean
[ %{buildroot} != "/" ] && rm -rf %{buildroot}

%files
/usr/bin/create-image
/usr/bin/dib-run-parts
/usr/share/CreateImage

%changelog
* Mon May 27 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.0.0-7
- ID:NA
- SUG:NA
- DESC: Add basic support for riscv64

* Mon Apr 29 2024 wangchong<wangchong56@huawei.com> - 1.0.0-6
- ID:NA
- SUG:NA
- DESC: fix make image in docker

* Wed Oct 25 2023 wangchong<wangchong56@huawei.com> - 1.0.0-5
- ID:NA
- SUG:NA
- DESC: add some packages in rpmlist

* Wed Aug 16 2023 wangchong<wangchong56@huawei.com> - 1.0.0-4
- ID:NA
- SUG:NA
- DESC: fix issue I7T50T

* Mon Jul 10 2023 WenlongZhang<zhangwenlong@loongson.cn> - 1.0.0-3
- ID:NA
- SUG:NA
- DESC:Add loongarch64 support for CreateImage

* Mon Jul 25 2022 wangchong<wangchong56@huawei.com> - 1.0.0-2
- ID:NA
- SUG:NA
- DESC: fix issue I5GK9F

* Fri May 27 2022 wangchong<wangchong56@huawei.com> - 1.0.0-1
- ID:NA
- SUG:NA
- DESC: package init
